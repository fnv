import nil.build

project = 'fnv'

builder = nil.build.builder(project)
builder.include(project)
builder.static_library()
