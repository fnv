#include <fnv/fnv.hpp>
#include <fnv/fnv_hash.hpp>

uword fnv1a_hash(void const * input, std::size_t size, uword previous_hash_value)
{
#ifdef AIL_64
	return static_cast<uword>(fnv_64a_buf(const_cast<void *>(input), size, static_cast<Fnv64_t>(previous_hash_value)));
#else
	return static_cast<uword>(fnv_32a_buf(const_cast<void *>(input), size, static_cast<Fnv32_t>(previous_hash_value)));
#endif
}
