#include <ail/types.hpp>
#include <ail/environment.hpp>

#ifdef AIL_64
#define FNV_INIT 14695981039346656037u
#else
#define FNV_INIT 2166136261u
#endif

uword fnv1a_hash(void const * input, std::size_t size, uword previous_hash_value = FNV_INIT);

#undef FNV_INIT
